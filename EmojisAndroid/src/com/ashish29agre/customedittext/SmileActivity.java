package com.ashish29agre.customedittext;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.os.Bundle;
import android.text.Spannable;
import android.text.Spannable.Factory;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SmileActivity extends Activity {

	private TextView smilesTextView;
	private EditText smileEditText;
	private Button button;
	private Button showKeyboard;
	private Button hideKeyboard;
	private Keyboard keyboard;
	private SmileKeyboardView keyboardView;
	private OnKeyboardActionListener keyboardActionListener = new OnKeyboardActionListener() {

		@Override
		public void swipeUp() {
			// TODO Auto-generated method stub

		}

		@Override
		public void swipeRight() {
			// TODO Auto-generated method stub

		}

		@Override
		public void swipeLeft() {
			// TODO Auto-generated method stub

		}

		@Override
		public void swipeDown() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onText(CharSequence text) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onRelease(int primaryCode) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onPress(int primaryCode) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onKey(int primaryCode, int[] keyCodes) {
			Log.d("TAG", "" + primaryCode);
			updateEdiText("8)");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_smile);
		smileEditText = (EditText) findViewById(R.id.smile_contents);
		smilesTextView = (TextView) findViewById(R.id.converted_smiles);
		button = (Button) findViewById(R.id.btn_convert);
		showKeyboard = (Button) findViewById(R.id.btn_showkeyboard);
		hideKeyboard = (Button) findViewById(R.id.btn_hidekeyboard);
		keyboard = new Keyboard(this, R.xml.keyboard_layout);
		keyboardView = (SmileKeyboardView) findViewById(R.id.keyboardview);
		keyboardView.setKeyboard(keyboard);
		keyboardView.setPreviewEnabled(false);
		keyboardView.setOnKeyboardActionListener(keyboardActionListener);
	}

	public void onClick(View v) {
		Spannable spannable = getSmiledText(this, smileEditText.getText()
				.toString());
		smileEditText.setText("");
		smileEditText.append(spannable);
		smilesTextView.setText("");
		smilesTextView.append(spannable);
	}

	private void updateEdiText(String text) {
		smileEditText.append(text);
		Spannable spannable = getSmiledText(this, smileEditText.getText().toString());
		smileEditText.setText("");
		smileEditText.append(spannable);
		smilesTextView.setText("");
		smilesTextView.append(spannable);
	}

	public void showKeyboard(View v) {
		showCustomKeyboard(v);
	}

	public void hideKeyboard(View v) {
		hideCustomKeyboard();
	}

	public void hideCustomKeyboard() {
		keyboardView.setVisibility(View.GONE);
		keyboardView.setEnabled(false);
	}

	public void showCustomKeyboard(View v) {
		keyboardView.setVisibility(View.VISIBLE);
		keyboardView.setEnabled(true);
		if (v != null)
			((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE))
					.hideSoftInputFromWindow(v.getWindowToken(), 0);
	}

	public boolean isCustomKeyboardVisible() {
		return keyboardView.getVisibility() == View.VISIBLE;
	}

	private static final Factory spannableFactory = Spannable.Factory
			.getInstance();

	private static final Map<Pattern, Integer> emoticons = new HashMap<Pattern, Integer>();

	static {
		addPattern(emoticons, "8)", R.drawable.a);
		addPattern(emoticons, ":(", R.drawable.b);
	}

	private static void addPattern(Map<Pattern, Integer> map, String smile,
			int resource) {
		map.put(Pattern.compile(Pattern.quote(smile)), resource);
	}

	public static boolean addSmiles(Context context, Spannable spannable) {
		boolean hasChanges = false;
		for (Entry<Pattern, Integer> entry : emoticons.entrySet()) {
			Matcher matcher = entry.getKey().matcher(spannable);
			while (matcher.find()) {
				boolean set = true;
				for (ImageSpan span : spannable.getSpans(matcher.start(),
						matcher.end(), ImageSpan.class))
					if (spannable.getSpanStart(span) >= matcher.start()
							&& spannable.getSpanEnd(span) <= matcher.end())
						spannable.removeSpan(span);
					else {
						set = false;
						break;
					}
				if (set) {
					hasChanges = true;
					spannable.setSpan(new ImageSpan(context, entry.getValue()),
							matcher.start(), matcher.end(),
							Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
				}
			}
		}
		return hasChanges;
	}

	public static Spannable getSmiledText(Context context, CharSequence text) {
		Spannable spannable = spannableFactory.newSpannable(text);
		addSmiles(context, spannable);
		return spannable;
	}
}
